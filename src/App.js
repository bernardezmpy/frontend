import {BrowserRouter as Router, Routes, Route, Navigate} from 'react-router-dom';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext'

import AppNavbar from './components/AppNavbar';
import AdminViewAll from './components/AdminViewAll';

import Home from './pages/Home';
import SignUp from './pages/SignUp';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error404 from './pages/Error404';

import ViewActive from './pages/ViewActive';
import ViewSpecific from './pages/ViewSpecific';
import Cart from './pages/Cart'

import AdminDashboard from './pages/AdminDashboard';
import CreateProduct from './pages/CreateProduct';
import UpdateProduct from './pages/UpdateProduct';



import './App.css';


function App() {

  const [user, setUser] = useState(null);
  const unsetUser = () => {
    localStorage.removeItem("token");
  }

  /*useEffect(() => {
    console.log(user)
  }, [user])*/

  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/user/profile`, {
      headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}
    })
      .then(response => response.json())
      .then(data => {
        console.log(data)

        if(false) {
          setUser({id: data._id, isAdmin: data.isAdmin})
        } else {
          setUser(null)
        }
        
      })
  }, [])


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
          <AppNavbar/>
            <Routes>
              <Route path = "/" element = {<Home/>}/>
              <Route path = "/signup" element = {user ? <Navigate to = "/" />: <SignUp/>}/>
              <Route path = "/login" element = {<Login/>}/>
              <Route path = "/logout" element = {<Logout/>}/>

              <Route path = "/viewProducts" element = {<ViewActive/>}/>
              <Route path = "/viewSpecific/:productId" element = {<ViewSpecific/>}/>
              <Route path = "/cart" element = {<Cart/>}/>
              
              <Route path = "/adminDashboard" element = {<AdminDashboard/>}/>
              <Route path = "/adminCreate" element = {<CreateProduct/>}/>
              <Route path = "/adminViewAll" element = {<AdminViewAll/>}/>
              <Route path = "/adminUpdate/:productId" element = {<UpdateProduct/>}/>

              <Route path = "*" element = {<Error404/>}/>

            </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
