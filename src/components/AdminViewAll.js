//Admin View All Products (Parent)

import { useEffect, useState } from "react"
import AdminViewAllMod from "./AdminViewAllMod";
import {Fragment} from 'react';

const AdminViewAll = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
      const fetchProducts = async () => {
        const response = await fetch(`${process.env.REACT_APP_URI}/products/allProducts`, {
        headers: {
            /*'Content-Type' : 'application/json',*/
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
    })

        const json = await response.json(); 
        if (!response.ok) {
          console.log(json.error);
        }

        if (response.ok) {
            setProducts(json.map(product => {
            return (<AdminViewAllMod key={product._id} product={product}/>)
        }))
        }
      };

      fetchProducts();
    }, []);


  /*useEffect(() => {

    fetch(`${process.env.REACT_APP_URI}/products/allProducts`, {
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);

        setProducts(data.map(product => {
            return (<AdminViewAllMod key={product._id} product={product}/>)
        }))
    }).catch(error => {
                console.log(error);
               
            })

  }, []);*/

  return (
    <Fragment>
        {products}
    </Fragment>
  )
}

export default AdminViewAll