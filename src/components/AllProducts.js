//Admin Dashboard Page: Retrieve all products (Child)

import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom';
import { useUserContext } from '../hooks/useUserContext';

export default function AllProducts({productsProp}) {
	console.log(productsProp)
	console.log("FUNCTION", productsProp._id)

	const {_id, category, name, description, price, stocks} = productsProp;

	const [endUsers, setEndUsers] = useState(0);
	const [stocksAvailable, setStocksAvailable] = useState(0)
	const [isAvailable, setIsAvailable] = useState(true)
	
	const {user} = useUserContext();


	useEffect(()=>{
		if(stocksAvailable === 0){
		setIsAvailable(false)
		}

	}, [stocksAvailable])


	function purchase(){

		if(stocksAvailable === 1){
			alert("Congratulations!")
		} 
		
		setEndUsers(endUsers + 1)
		setStocksAvailable(stocksAvailable - 1)
	}

	function goTo(){
		console.log("HELLO WORLD")
	}


	return (
		<Container>
			<Row className= 'my-5'>
				<Col xs={12} md={4} className='offset-md-4 offset-0'>
					<Card className="bg-light p-3 text-dark bg-opacity-50">
					      <Card.Body>
					        <h4>{name}</h4>
					        <h5>PHP {price}</h5>
					        <hr></hr>
					        <Button
					         	as={Link} to={`/viewSpecific/${_id}`}
						        variant="outline-light" className="yellow1 fw-bold text-dark shadow-sm"
						        >
					        Details</Button>
					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		
		)
}

