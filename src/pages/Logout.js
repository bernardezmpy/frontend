//Logout Page: Logout Admin and User

import {Navigate} from 'react-router-dom';
import { useEffect} from 'react';
import Swal from 'sweetalert2';
import { useUserContext } from '../hooks/useUserContext';

export default function Logout(){

	const{removeUser} = useUserContext();
	
	

	useEffect(() => {
		removeUser();
	}, [])

	Swal.fire({
		title: "Thank you!",
		icon: "info",
		text: "Hope to serve you again soon"
		
	})

	return(
		<Navigate to = "/login" />
		)
}