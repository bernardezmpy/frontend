//Admin Dashboard Page: Update Product information

import {Button, Col, Row, Container, Card} from "react-bootstrap";
import {useState, useEffect} from "react";
import {useParams, useNavigate} from "react-router-dom";
import Form from "react-bootstrap/Form";
import Swal from "sweetalert2";
import { useUserContext } from '../hooks/useUserContext';


export default function UpdateProduct() {
  const [category, setCategory] = useState("");
  const [image, setImage] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [stocks, setStocks] = useState("");
  const [isActive, setIsActive] = useState(true);
  const {productId} = useParams();
  
  const {user} = useUserContext();
  const navigate = useNavigate();


  useEffect(() => {
    if (
      category !== "" && image !== "" && name !== "" && description !== "" && price !== "" && stocks !== ""){
      setIsActive(false);
    } else {
      setIsActive(true);
    }
  }, [category, image, name, description, price, stocks]);


  async function updateProduct(event) {
    event.preventDefault();
    console.log(productId)

    const response = await fetch(
        `${process.env.REACT_APP_URI}/products/updateProduct/${productId}`, {
          method: "PUT",
          headers: {"Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`
          },
        body: JSON.stringify({
        category: category,
        image: image,
        name: name,
        description: description,
        price: price,
        stocks: stocks
        })
      });

      const json = await response.json();
      console.log(json)

      if (!response.ok) {
        Swal.fire({
            title: "Update Failed",
            icon: "error",
            text: "Please check if the information you input are correct.",
          });
        
        console.log(json.error);
        
      }

      if (response.ok) {
        /*productsDispatch({ type: 'SET_ACTIVE_PRODUCTS', payload: json });*/
        Swal.fire({
            title: "Product Successfully Updated",
            icon: "success",
            text: "Product information has been updated",
          });

        navigate("/adminDashboard")
      }
  }

  return (
    <Container className = "my-4">
      <Row className="mt-5">
        <Col className = "col-md-4 col-8 offset-md-4 offset-2">
          
          <Card className="p-4 rounded shadow-lg">
            <Form onSubmit={updateProduct} className="p-3">
                <Form.Group controlId = "productCategory">
                  <Form.Label className = "fw-bold">Category</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "text"
                    placeholder = "Update Product Category"
                    value = {category}
                    onChange = {event => setCategory(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productName">
                  <Form.Label className = "fw-bold">Name</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "text"
                    placeholder = "Update Product Name"
                    value = {name}
                    onChange = {event => setName(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productPrice">
                  <Form.Label className = "fw-bold">Price</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "number"
                    placeholder = "Update Price"
                    value = {price}
                    onChange = {event => setPrice(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productStock">
                  <Form.Label className = "fw-bold">Count in Stock</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "number"
                    placeholder = "Update Stocks"
                    value = {stocks}
                    onChange = {event => setStocks(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productDescription">
                  <label
                    for="exampleFormControlTextarea1"
                    class="form-label"
                    className = "fw-bold">Description</label>
                  <textarea
                    class="form-control"
                    id="exampleFormControlTextarea1"
                    rows="3"
                    value = {description}
                    placeholder = "Update Description"
                    onChange = {event => setDescription(event.target.value)}
                    required></textarea>
                </Form.Group>

                <Form.Group controlId = "productImage" className="pb-3">
                  <Form.Label className = "fw-bold">Image</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "url"
                    placeholder = "Update Image URL"
                    value = {image}
                    onChange = {event => setImage(event.target.value)}/>
                </Form.Group>

                <Button
                  className = "yellow1 text-black shadow-sm"
                  variant = "outline-warning"
                  type = "submit">
                  Update Product
                </Button>
            </Form>
          </Card>
          
        </Col>
      </Row>
    </Container>
  );
}
