import {Fragment} from 'react';
import {Link} from 'react-router-dom';
import {Container} from 'react-bootstrap';

export default function Error404 () {
  return (
    <Fragment>
   		<Container className="text-light pt-4">
   			<h2>Error: Page Not Found</h2>
			{"Go back to the "} 
			<Link to = "/adminDashboard">homepage.</Link>
    	</Container>
    </Fragment>
  );
}

