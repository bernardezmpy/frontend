//Admin Dashboard Page: Create Product 

import {Container, Row, Col, Button, Form, Card} from 'react-bootstrap';
import React, {useState, useEffect, useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2';
import { useUserContext } from '../hooks/useUserContext';


export default function CreateProduct(){
	
	const [category, setCategory] = useState("");
	const [image, setImage] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [isActive, setIsActive] = useState(false);
	
	const {user} = useUserContext();
	const navigate = useNavigate();

	useEffect(() => {
		if(category !== "" && image !== "" && name !== "" && description !== "" && price !== 0 && stocks !== 0){
			setIsActive(true);
		}

		else{
			setIsActive(false);
		}

	}, [category, image, name, description, price, stocks]);


	const createProduct = (event) => {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_URI}/products/`, {
			method: "POST",
			headers: {"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`},
			body: JSON.stringify({
				category,
				image,
				name,
				description,
				price,
				stocks
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			console.log(2)

			if(data){
				Swal.fire({
					title: "Product successfully created",
					icon: "success",
					text: "New product created"
				})

				navigate('/');
			}

			else{
				Swal.fire({
					title: "Product already exists",
					icon: "error",
					text: "The product you are trying to create is already existing."
				})
			}
		})
	}


	return (
		<Container className = "my-4">
			<Row className="mt-5">
				<Col className = "col-md-4 col-8 offset-md-4 offset-2">

					<Card className="p-4 rounded shadow-lg">
						<Form onSubmit = {createProduct} className="p-3">
					      <Form.Group controlId = "productCategory">
					      	<Form.Label className = "fw-bold">Category</Form.Label>
					      	<Form.Control
					      		className = "mb-3"
					      		type = "text"
					      		placeholder = "Create New Product Category"
					      		value = {category}
					      		onChange = {event => setCategory(event.target.value)}
					      		required/>
					      </Form.Group>

					      <Form.Group controlId = "productName">
					      	<Form.Label className = "fw-bold">Name</Form.Label>
					      	<Form.Control
					      		className = "mb-3"
					      		type = "text"
					      		placeholder = "Create New Product Name"
					      		value = {name}
					      		onChange = {event => setName(event.target.value)}
					      		required/>
					      </Form.Group>

					      <Form.Group controlId = "productPrice">
					      	<Form.Label className = "fw-bold">Price</Form.Label>
					      	<Form.Control
					      		className = "mb-3"
					      		type = "number"
					      		placeholder = "PHP"
					      		value = {price}
					      		onChange = {event => setPrice(event.target.value)}
					      		required/>
					      </Form.Group>

					      <Form.Group controlId = "productStock">
					      	<Form.Label className = "fw-bold">Count in Stock</Form.Label>
					      	<Form.Control
					      		className = "mb-3"
					      		type = "number"
					      		placeholder = "0"
					      		value = {stocks}
					      		onChange = {event => setStocks(event.target.value)}
					      		required/>
					      </Form.Group>

					      <Form.Group controlId = "productDescription">
					      	<label
			                    for="exampleFormControlTextarea2"
			                    class="form-label"
			                    className = "fw-bold">Description</label>
					      	<textarea
					      		class="form-control"
			                    id="exampleFormControlTextarea2"
			                    rows="3"
					      		placeholder = "Create New Product Description"
					      		value = {description}
					      		onChange = {event => setDescription(event.target.value)}
					      		required></textarea>
					      </Form.Group>

					      <Form.Group controlId = "productImage" className="pb-3">
					      	<Form.Label className = "fw-bold">Image</Form.Label>
					      	<Form.Control
					      		className = "mb-3"
					      		type = "url"
					      		placeholder = "Insert New Image URL"
					      		value = {image}
					      		onChange = {event => setImage(event.target.value)}
					      		required/>
					      </Form.Group>
					      
					      <Button
					      	className = "yellow1 text-black shadow-sm"
					      	variant = "outline-warning"
					      	type = "submit">
					        Publish Now
					      </Button>
					</Form>
					</Card>
					
				</Col>
			</Row>
		</Container>
		)
}