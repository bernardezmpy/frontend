// Login Page: Login Admin and User

import {Container, Row, Col, Button, Form, Card} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import { useUserContext } from '../hooks/useUserContext';
import Swal from 'sweetalert2';

export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);
	const {user, setUser} = useUserContext()
	const navigate = useNavigate();

	useEffect(() => {
		if(email !== "" & password !== ""){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [email, password])


	function loginUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_URI}/user/loginUser`, {
			method: "POST",
			headers: {'Content-Type' : 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
			.then(response => response.json())
			.then(data => {
				console.log(data)

			if(data.accessToken !== "empty"){
				localStorage.setItem("token", data.accessToken);
				retrieveUserDetails(data.accessToken);
				Swal.fire({
					title: "Login Successfully",
					icon: "success",
					text: "Welcome to The House of Crust!"
				})
					console.log("@LOGIN", user)

			} else{
				Swal.fire({
					title: "Uh-oh, the email or password is incorrect",
					icon: "error",
					text: "You need to input the correct email address or password to proceed."
				})
				setPassword("")
			}
		})

		const retrieveUserDetails = (token) => {
			fetch(`${process.env.REACT_APP_URI}/user/profile`, {
				headers: {Authorization: `Bearer ${token}`}
			})
				.then(response => response.json())
				.then(data => {
					console.log(data)
				
				setUser({id: data._id, isAdmin: data.isAdmin})
				console.log(user)
				user && user.isAdmin ? navigate("/adminDashboard") : navigate("/")
			})
		}
	}

	return(
		user ?
		<Navigate to = "/" />
		:

		<Container>
			<Row>
				<Col className = "col-md-4 col-8 offset-md-4 offset-2 mt-5">
					<Card className = "cardHighlight p-4 mx-2">
						<div className = "p-2">
							<h2>Welcome back!</h2>
							<h6>Please sign in to your account</h6>
						</div>

						<Form
						className = "p-2"
						onSubmit = {loginUser}>

							<Form.Group className="mb-3" controlId="email">
							  <Form.Control
							  	type="email"
							  	placeholder="Email Address"
							  	value = {email}
							  	onChange = {event => setEmail(event.target.value)}
							  	required/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="password">
							  <Form.Control
							  	type="password"
							  	placeholder="Password"
							  	value = {password}
							  	onChange = {event => setPassword(event.target.value)}
							  	required/>
							</Form.Group>

							<div id="link-register">Not yet registered? <Link id="register" to="/signup">Register</Link> here.</div>

							<br></br>
							<Button
								className = "shadow-sm fw-bold my-2"
								variant="warning"
								type="submit"
								disabled = {!isActive}>
							  Login
							</Button>
						</Form>
					</Card>

					
				</Col>
			</Row>
		</Container>

		)

}